# SWRM - rozpoznawanie obiektów w chmurze punktów

Celem tego ćwiczenia jest zaimplementowanie algorytmu rozpoznawania obiektów w chmurze punktów. Przygotowany pakiet `swrm_rosbot_pointcloud` jest modyfikacją ćwiczenia wykonywanego w połączeniu z rzeczywistym robotem przemysłowym Fanuc, do którego instrukcja znajduje się tu: [doc/lab4.pdf](src/swrm_rosbot_pointcloud/doc/lab4.pdf). W tej wersji ćwiczenia jest wykorzystany symulator robota *Husarion ROSbot2.0*.

W pakiecie `swrm_rosbot_pointcloud` przygotowano węzeł ROS, który pozwala na odebranie i wstępne przetworzenie chmury punktów oraz sterowanie robotem *ROSbot*.

Zadanie polega na modyfikacji kodu pozwalającej odnaleźć w chmurze punktów kosz na śmieci znajdujący się w symulowanym świecie, a następnie podjazd robotem w okolicę środka obiektu. W wersji na ocenę maksymalną, przed dojazdem do kosza na śmieci, należy również rozpoznać i podjechać do większej z dwóch puszek.

## Instalacja

### Instalacja symulacji ROSbota

> Ten punkt można pominąć jeżeli był już wykonany wcześniej.

Tworzenie przestrzeni roboczej:

```
cd ~
mkdir ros_workspace
mkdir ros_workspace/src
cd ~/ros_workspace/src
catkin_init_workspace
cd ~/ros_workspace
catkin_make
```

Klonowanie repozytorium:

```
cd ~/ros_workspace/src
git clone https://github.com/husarion/rosbot_description.git
```

Instalowanie zależności:

```
cd ~/ros_workspace
rosdep install --from-paths src --ignore-src -r -y
```

Budowanie przestrzeni roboczej:

```
cd ~/ros_workspace
catkin_make
```

Załączanie do konsoli ustawień przestrzeni roboczej:

```
source ~/ros_workspace/devel/setup.sh
```

### Instalacja `swrm_rosbot_pointcloud`

Klonowanie repozytorium:

```
cd ~/ros_workspace/src
git clone https://bitbucket.org/rrgwut/swrm_rosbot_pointcloud.git
```

Budowanie przestrzeni roboczej:

```
cd ~/ros_workspace
catkin_make
```

## Uruchomienie

Uruchomienie symulatora `Gazebo` i `rviz`'a:

```
roslaunch swrm_rosbot_pointcloud setup.launch
```

Następnie z poziomu środowiska programistycznego (np. VS Code) uruchamiamy węzeł `swrm_rosbot_pointcloud_node`.

Po uruchomieniu węzła pojawi się okno `Point Cloud Segmentation Viewer`, w którym wyświetlane będą odfiltrowane chmury punktów. Robot będzie obracał się w lewo z niedużą prędkością.

Sterowanie i filtracja jest realizowane w metodzie [`PointCloudObjectRecognition::processPointCloud`](src/swrm_rosbot_pointcloud/src/swrm_rosbot_pointcloud/PointCloudObjectRecognition.cpp#L40), której implementację należy dokładnie prześledzić i odpowiednio zmodyfikować.
