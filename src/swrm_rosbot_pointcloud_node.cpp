#include <cstdlib>
#include <ros/ros.h>

#include "swrm_rosbot_pointcloud/PointCloudObjectRecognition.h"

/*
 *
 */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "swrm_rosbot_pointcloud_node");

    PointCloudObjectRecognition object_recognition;

    ros::Rate loop_rate(20);

    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}